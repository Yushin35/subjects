import openpyxl as x
import datetime
import sys

if __name__ == '__main__':
    DATE = 2
    C_NAME = 4
    USD_RATES = 4
    PRODUCT = 5
    PAY = 8
    month = '02'
    prod = 'АРМАТУРА'
    if len(sys.argv) == 2:
        month = sys.argv[1]
    elif len(sys.argv) == 3:
        month = sys.argv[1]
        prod = sys.argv[2]
    wb = x.load_workbook('/home/web/python/envs/testA/Test_Case.XLSX')          #загрузка файла
    BRDN = wb['BRDN']                                                           #Получение страниц
    rates = wb['x-rates']
    rates_data = list(rates.rows)[6:]                                           #извлечение столбцов для
    usd_rates = {str(x[3].value): round(x[USD_RATES].value, 6) for x in rates_data}     #значения курса в долларах по каждой дате ключ является строкой даты

    bd_data = BRDN[4:len(list(BRDN))]                                           #извлечение информативных полей для основной информации
    company_names = set([x[C_NAME].value for x in bd_data if x[C_NAME].value is not None])#Извлечение наименований компаний

    company_names_dict = dict.fromkeys(company_names, 0)                        #Словарь с суммой по каждой компании

    sep_data = [x for x in bd_data if str(x[DATE].value)[5:7] == month and x[PRODUCT].value == prod]

    def get_currency_ex(curr_table, date):

        one_day_seconds = 24*60*60
        seconds = date.timestamp()
        day_week = date.weekday()
        if day_week == 5:
            seconds -= one_day_seconds
        elif day_week == 6:
            seconds -= 2*one_day_seconds
        new_date = str(datetime.datetime.fromtimestamp(seconds))

        return curr_table[new_date]


    for item in sep_data:
        date = item[DATE].value
        company_name = item[C_NAME].value
        pay_val = item[PAY].value
        try:
            curr_exc = round(get_currency_ex(usd_rates, date), 6)
            company_names_dict[company_name] += round((pay_val / curr_exc), 6)
        except:
            print('some error')

    company_names_dict['X'] = sum(list(company_names_dict.values()))
    file = open('result.txt', 'w')      #запись ответов в файл
    for key in company_names_dict:
        print('%s => %s' % (key, round(company_names_dict[key], 6)), file=file)
    print(company_names_dict)
