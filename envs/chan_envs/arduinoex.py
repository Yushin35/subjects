import pyfirmata, time
pin = 13
port = '/dev/ttyACM0'
board = pyfirmata.Arduino(port)
count = 0
while True:
    print(time.ctime(time.time()))
    count+=1
    board.digital[pin].write(1)
    print('pin({0})->status({1})'.format(pin, board.digital[pin].read()))
    time.sleep(0.1)
    board.digital[pin].write(0)
    print('pin({0})->status({1})'.format(pin, board.digital[pin].read()))
    time.sleep(0.1)
    if count>15:
        break
board.exit()
