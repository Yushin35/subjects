#!/usr/bin/python
def more(text, numlines=15):
    data = text.splitlines();
    while data:
        chunk = data[:numlines]
        data = data[numlines:]
        for line in chunk:
            print(line)
        if input("More?\n") not in ['Y', 'y'] or len(data) == 0: break


if __name__ == "__main__":
    import sys
    more(open(sys.argv[1]).read(), int(sys.argv[2]))
