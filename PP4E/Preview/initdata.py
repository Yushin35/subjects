bob = dict(name='Bob Smith', age=42, pay=42000, job='dev')
john = dict(name='John Connor ', age=302, pay=20, job='killer')
sara = dict(name='Sara Connor', age=24, pay=0, job='mom')

db = {}
db['bob'] = bob
db['john'] = john
db['sara'] = sara

if __name__ == '__main__':
    for key in db.keys():
        print(key, '=>\n', db[key])

