import pickle
dbfile = open('people-pickle', 'rb')
db = pickle.load(dbfile)
dbfile.close()

db['john']['name'] = 'new name'

dbfile = open('people-pickle', 'wb')
pickle.dump(db, dbfile)
dbfile.close()

