import pickle, glob
for filename in glob.glob('*.pcl'):
    recfile = open(filename, 'rb')
    record = pickle.load(recfile)
    recfile.close()
    print(filename, '=>\n', record)
