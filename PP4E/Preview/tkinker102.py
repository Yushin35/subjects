from tkinter import *
from tkinter.messagebox import showinfo

class MyGui(Frame):
    def __init__(self, parent=None):
        Frame.__init__(self, parent)
        label = Label(self, text="Enter your name:" )
        label.pack(side=TOP)
        ent = Entry(self)
        ent.pack(side=TOP)
        button = Button(self, text="Submit", command=(lambda: self.reply(ent.get())))
        button.pack()

    def reply(self, name):
        showinfo(title="title", message='Hello:'+name)

if __name__ == "__main__":
    window = MyGui()
    window.pack()
    window.mainloop()
