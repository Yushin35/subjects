import sys
from tkinter import *


def someFunc():
    print('btn clicked')


def printEvent(event):
    print(event.x)
    print(event.y)

root1 = Frame()

#widget = Button(root, text='Bye tkinter world', command=sys.exit)
btn_1 = Button(root1, text='Bye tkinter world', command=sys.exit)
btn_2 = Button(root1, text='btn2')
btn_2.bind('<Button-1>', printEvent)
btn_2.pack(side=TOP, expand=NO, fill=X)
btn_1.pack(side=TOP, expand=NO, fill=X)
root1.pack(expand=YES, fill=BOTH)
root1.mainloop()
#destroy закроет только одно окно а  quit закрывает все окна