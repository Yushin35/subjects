from tkinter import *


class Window():
    def __init__(self, parent=None):
        self.widget = Frame(parent)
        self.widget.pack()
        self.data = 0
        self.makeWidget()

    def makeWidget(self):

        self.btn = Button(text='show', command=self.showMessage)
        self.btn.pack(side=TOP)

    def showMessage(self):
        print('Number ', self.data)
        self.data += 1

Window().widget.mainloop()