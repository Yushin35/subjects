import os, sys


def recur(currdir):
    print('['+currdir+']')
    for file in os.listdir(currdir):
        path = os.path.join(currdir, file)
        if not os.path.isdir(path):
            print(path)
        else:
            recur(path)

if __name__ =="__main__":
    recur(sys.argv[1])