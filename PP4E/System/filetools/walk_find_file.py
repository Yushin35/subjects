import sys, os

def find(root, filename):
    res = []
    for (thisdir, subshere, fileshere) in os.walk(root):
        print('['+thisdir+']')
        for fname in fileshere:
            if filename == fname:
                path = os.path.join(thisdir, fname)
                res.append(path)

    return res
if __name__=="__main__":
    for i in find(sys.argv[1], sys.argv[2]):
        print("Path:" + i)