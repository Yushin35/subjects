import _thread
stdoutmutex = _thread.allocate()
exitmutes = [False]*10

def counter(myId, count):
    for i in range(count):
        stdoutmutex.acquire()
        print("[%s] => [%s]"%(myId, count))
        stdoutmutex.release()
    exitmutes[myId] = True

for i in range(10):
    _thread.start_new_thread(counter, (i, 5))
while False in exitmutes: pass

print("Main thread")