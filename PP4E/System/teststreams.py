def interact():
    while True:
        try:
            reply = input('Enter a number=>')
        except EOFError:
            break
        else:
            num = int(reply)
            print('%d cube is %d' % (num, num**3))

if __name__ == '__main__':
    interact()