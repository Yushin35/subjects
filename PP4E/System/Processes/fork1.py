import os

def child():
    print("Helo from child ", os.getpid())
    os._exit(0)

def parent():
    while True:
        newpid = os.fork()
        print(newpid)
        if newpid == 0:
            child()
        else:
            print("Helo from parent ", os.getpid(), newpid)
        if input() == 'q': break

parent()