def scaner(fin, fout, function):
    filein = open(fin, 'r')
    fileout = open(fout, 'w')

    while True:
        line = filein.readline()
        if not line: break
        fileout.write(function(line))
    filein.close()
    fileout.close()


def addMr(line):
    return "Mr." + line


def createTestFile():
    file = open('testIn.txt', 'w')
    names = ["John", "Serg", "Clay", "Van"]
    for name in names:
        file.write(name + "\n")
    file.close()

if __name__ == "__main__":
    createTestFile()
    scaner('testIn.txt', 'testOut.txt',  addMr)
    f = open('testOut.txt', 'rb')
    #print(f.read())