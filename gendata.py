from random import randrange, choice
from string import ascii_lowercase as ls
from time import ctime

maxint = 2*6400000000
tlds = ('com', 'edu','net', 'org', 'gov')

for i in range(randrange(1000)):
    dtint = randrange(100, maxint)
    dtstr = ctime(dtint)
    llen = randrange(4, 10)
    login = ''.join(choice(ls) for j in range(llen))
    dlen = randrange(llen, 15)
    dom = ''.join(choice(ls) for j in range(dlen))
    print('%s::%s@%s.%s::%d-%d-%d'%(dtstr, login, dom, choice(tlds),
                                    dtint, llen, dlen))
