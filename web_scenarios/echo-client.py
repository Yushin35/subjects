#!/usr/bin/python
import sys
from socket import *


host = 'localhost'
port = 7774
msg = [b'msg to server']
if len(sys.argv) > 1:
    host = sys.argv[1]
    if len(sys.argv) > 2:
        msg = (x.encode() for x in sys.argv[2:])
socketObj = socket(AF_INET, SOCK_STREAM)
socketObj.connect((host, port))
for line in msg:
    socketObj.send(line)
    print('waiting response...')
    data = socketObj.recv(1024)
    print('Client recieved', data)
socketObj.close()
