#!/usr/bin/python
import sys
from socket import *


host = 'localhost'
port = 50008
msg = [b'msg to server']
if len(sys.argv) > 1:
    host = sys.argv[1]
    if len(sys.argv) > 2:
        msg = (x.encode() for x in sys.argv[2:])
socketObj = socket(AF_INET, SOCK_STREAM)
socketObj.connect((host, port))
while True:
    line = input('=>')
    socketObj.send(line.encode())
    data = socketObj.recv(1024)
    print(data.decode())
socketObj.close()
