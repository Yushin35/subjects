import time
import os
from socket import *
import time
import _thread as thread

myHost = ''
myPort = 50008
#  создать обьект сокета tcp
sockObj = socket(AF_INET, SOCK_STREAM)
#  связатся с номером порта сервера
sockObj.bind((myHost, myPort))
sockObj.listen(5)

d = {'date': (lambda: time.ctime(time.time())),
     'ls': (lambda: ', '.join(os.listdir(os.getcwd()))),
     'cd': (lambda dir: os.chdir(dir)),
     'cwd': (lambda: os.getcwd())}


def now():
    return time.ctime(time.time())


def handleClient(connection, address):
    while True:
        data = connection.recv(1024)
        if not data:
            break
        data = data.decode()
        if 'cd' in data:
            dir1 = data.split()[1]
            print(dir1)
            f = d.get('cd', None)
            f(dir1)
            connection.send(os.getcwd().encode())
        else:
            f = d.get(data, None)
            if f is not None:
                reply = f()
            else:
                reply = "None"
            connection.send(reply.encode())
    connection.close()
    print('closed connection...')


def dispatcher():
    global users, count
    while True:
        connection, address = sockObj.accept()
        print('Server connected by', address)
        print('at', now())
        thread.start_new_thread(handleClient, (connection, address))


if __name__ == '__main__':
    dispatcher()
