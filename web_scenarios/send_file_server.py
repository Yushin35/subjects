from socket import *
import _thread as thread
import time

HOST = ''
PORT = 7774
BUFSIZE = 2048

def now():
    return time.ctime(time.time())


def handleClient(connection, address):
    filename = connection.recv(BUFSIZE).decode()
    try:
        file = open(filename, 'rb')
        while True:
            bytes = file.read(BUFSIZE)
            if not bytes: break
            connection.send(bytes)
        print(filename, ' sended', 'at=>', now())
    except:
        print('server cannot open file=>%s' % (filename,))
    connection.close()


def startServer():
    sockObj = socket(AF_INET, SOCK_STREAM)
    sockObj.bind((HOST, PORT))
    sockObj.listen(5)
    print('Server start at ', now(), "\nPort ", PORT)
    while True:
        con, address = sockObj.accept()
        print('Connected to (%s at %s)'%(address, now()))
        thread.start_new_thread(handleClient, (con, address))


if __name__ == '__main__':
    startServer()