#!/usr/bin/python3
import os, http.cookies, time


cookstr = os.environ.get('HTTP_COOKIE')
cookies = http.cookies.SimpleCookie(cookstr)
usercook = cookies.get('visited')
print('Content-type: text/html\n')
print(usercook)