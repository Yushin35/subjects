#!/usr/bin/python
import sys
from socket import *
import  _thread as thread

def recieve(obj):
    while True:
        data = obj.recv(1024)
        print('to me:', data)

host = 'localhost'
port = 50007
msg = [b'msg to server']
if len(sys.argv) > 1:
    host = sys.argv[1]
    if len(sys.argv) > 2:
        msg = (x.encode() for x in sys.argv[2:])
socketObj = socket(AF_INET, SOCK_STREAM)
socketObj.connect((host, port))
thread.start_new_thread(recieve, (socketObj, ))
while True:
    line = input('=>')
    socketObj.send(line.encode())
socketObj.close()
