import time
from socket import *
import time
import _thread as thread

myHost = ''
myPort = 50007
#  создать обьект сокета tcp
sockObj = socket(AF_INET, SOCK_STREAM)
#  связатся с номером порта сервера
sockObj.bind((myHost, myPort))
sockObj.listen(5)


def now():
    return time.ctime(time.time())

def handleClient(connection, address):
    print(users)
    while True:
        data = connection.recv(1024)
        print("user", data)
        if not data:
            break
        dat = data.decode()
        to = dat.split(':')[0]
        message = dat.split(':')[1].encode()
        users[to].send(message)
    connection.close()
    print('closed connection...')

users = {}
count = 0
def dispatcher():
    global users, count
    while True:
        count+=1
        connection, address = sockObj.accept()
        print('Server connected by', address)
        print('at', now())
        users[str(count)] = connection
        thread.start_new_thread(handleClient, (connection, address))


if __name__ == '__main__':
    dispatcher()
