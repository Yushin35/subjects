import time
from socket import *
import time
import _thread as thread

myHost = ''
myPort = 50007
#  создать обьект сокета tcp
sockObj = socket(AF_INET, SOCK_STREAM)
#  связатся с номером порта сервера
sockObj.bind((myHost, myPort))
sockObj.listen(5)


def now():
    return time.ctime(time.time())


def handleClient(connection, address):
    while True:
        print('data processing...', address)
        time.sleep(2)
        data = connection.recv(1024)
        if not data:
            break
        x = int(data.decode())
        reply = 'Echo=>%s at %s' % (str(x**2), now())
        connection.send(reply.encode())
    connection.close()
    print('closed connection...')


def dispatcher():
    while True:
        connection, address = sockObj.accept()
        print('Server connected by', address)
        print('at', now())
        thread.start_new_thread(handleClient, (connection, address))


if __name__ == '__main__':
    dispatcher()
