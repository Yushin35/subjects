#!/usr/bin/python
from tkinter import *
import string
import random


PASSWORD_CHOICES = string.ascii_lowercase + '1234567890'


def generate_passsword(length):
    password = ''
    for _ in range(length):
        password += random.choice(PASSWORD_CHOICES)
    return password


def entry_handler(event):
    length_label['text'] = '123'
    length_out.delete(0, len(length_out.get()))
    length_out.insert(0, generate_passsword(int(length_in.get())))


root = Tk()

length_label = Label(root, text="Length of password")
password_label = Label(root, text="Generated password")

length_in = Entry(root)
length_out = Entry(root)

exit_btn = Button(root, text='Exit', command=sys.exit)
gen_btn = Button(root, text='Generate')

length_in.bind('<Return>', entry_handler)
gen_btn.bind('<Button-1>', entry_handler)

length_label.pack()
length_in.pack(fill=X)
password_label.pack()
length_out.pack(fill=X)
gen_btn.pack(side=TOP, fill=X)
exit_btn.pack(side=TOP,  fill=X)


root.mainloop()