#!/usr/bin/python
import sys
from socket import *


host = 'localhost'
port = 7774
msg = [b'msg to server']
if len(sys.argv) > 1:
    host = sys.argv[1]
    if len(sys.argv) > 2:
        msg = [x.encode() for x in sys.argv[2:]]
socketObj = socket(AF_INET, SOCK_STREAM)
socketObj.connect((host, port))
socketObj.send(msg[0])
print('waiting response...')
file = open('testServer.dat', 'wb')
try:
    bytesw = 0
    while True:
        data = socketObj.recv(2048)
        if not data: break
        bytesw += file.write(data)
        print('Client recieved', bytesw, 'bytes')
    file.close()
    print('file downloaded')
    socketObj.close()
except:
    print('Whats wrong')