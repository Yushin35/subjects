import random


def gen(count, a, b):
    return [random.randint(a, b) for _ in range(count)]
