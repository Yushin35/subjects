import pyfirmata, time
pin = 13
port = '/dev/ttyACM0'
board = pyfirmata.Arduino(port)
count = 0


def blink(count):
    for i in range(count):
        board.digital[pin].write(1)
        time.sleep(0.2)
        board.digital[pin].write(0)


while True:
    c = int(input('Enter num:'))
    if c == 0:
        board.exit()
        break
    blink(c)
