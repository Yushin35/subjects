"""
Соединим все вместе и доработаем до полной рабочей программы
"""
import math

class Stack:
    def __init__(self):
        self._l = []

    def push(self, item):
        self._l.append(item)

    def pop(self):
        return self._l.pop()

    def length(self):
        return len(self._l)

    def isEmpty(self):
        if not self.length():
            return True
        return False

def inf_postf(s):
    op = '+-*/$'
    st_out = Stack()
    st_tmp = Stack()
    uno_op = True
    for item in s:
        if item not in op and item not in '()':
            st_out.push(item)
            uno_op = False
        elif item in op:
            if item == '-' and uno_op:
                st_tmp.push('!')
                uno_op = False
            else:
                st_tmp.push(item)
            uno_op = False
        elif item == ')':
            if not st_tmp.isEmpty():
                st_out.push(st_tmp.pop())
            uno_op = False
        elif item == '(':
            uno_op = True
        else:
            uno_op = False
            st_out.push(st_tmp.pop())
    while not st_tmp.isEmpty():
        st_out.push(st_tmp.pop())
    result = []
    while not st_out.isEmpty():
        result.append(st_out.pop())
    result.reverse()
    return result

def calculate(s):
    math_operations = {'+': lambda ar1, ar2: ar1 + ar2,
                       '-': lambda ar1, ar2: ar1 - ar2,
                       '*': lambda ar1, ar2: ar1 * ar2,
                       '/': lambda ar1, ar2: ar2 / ar1,
                       '!': lambda ar1: ar1*(-1),
                       '$': lambda ar1: math.sqrt(ar1)}
    stack = Stack()
    for item in s:
        if item not in math_operations:
            stack.push(float(item))
        elif item == '!':
            ar1 = stack.pop()
            res = math_operations[item](ar1)
            stack.push(res)
        elif item == '$':
            ar1 = stack.pop()
            res = math_operations[item](ar1)
            stack.push(res)
        else:
            ar1 = stack.pop()
            ar2 = stack.pop()
            res = math_operations[item](ar1, ar2)
            stack.push(res)
    return stack.pop()

def printst(l):
    print(','.join(l))


def parseInput(s):
    tnumber = [str(x) for x in range(10)]
    tnumber.append('.')
    index = 0
    result = []
    while index < len(s):
        res = ''
        if s[index] in tnumber:
            while index < len(s) and s[index] in tnumber:
                res += s[index]
                index += 1
            result.append(res)
        else:
            result.append(s[index])
            index += 1
    return result

while True:
    parsedList = parseInput(input("[in]:"))
    print(parsedList)
    infixList = inf_postf(parsedList)
    printst(infixList)
    print('[out]:%s'%(calculate(infixList), ))


'((-(-1))+$(((-1)*(-1))-(4*(-1))))/2'
