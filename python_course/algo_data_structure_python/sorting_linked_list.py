#!/usr/bin/python3.5
"""
Task build program that generate random list and sort
in linkedList
"""

import random

class LList:
    def __init__(self, item, _next):
        self.item = item
        self._next = _next
tmp = LList('dummy', None)
dummy_tmp = tmp

sort_list = LList('dummy', None)
sort_root = sort_list

def fill(l, count):
    for i in range(count):
        r = random.randint(0, 1000)
        l._next = LList(r, None)
        l = l._next
def echo(l):
    while l != None:
        print(l.item)
        l = l._next
fill(tmp, 55)
l = dummy_tmp
echo(l)
unsorted_root = dummy_tmp._next
sorted_root = sort_root
while unsorted_root != None:
    print(unsorted_root.item, '----------122333')
    sorted_root = sort_root
    while True:
        if sorted_root._next == None:
            sorted_root._next = LList(unsorted_root.item, None)
            sorted_root = sorted_root._next
            break
        elif unsorted_root.item<=sorted_root._next.item:
            tmp = sorted_root._next
            sorted_root._next = LList(unsorted_root.item, tmp)
            break
        sorted_root = sorted_root._next
    unsorted_root = unsorted_root._next
echo(sort_root)
